import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gogame/presentation/theme/theme.dart';
import 'package:gogame/presentation/screens/main_screen.dart';
import 'package:gogame/presentation/screens/login_screen.dart';

const String _title = 'GoGame';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool? login = prefs.getBool("login");
  print(login);
  runApp(login == null ? MaterialApp(
      title: _title,
      home: const LoginScreen(),
      theme: getGoGameThemeData(),
      debugShowCheckedModeBanner: false)
      : MaterialApp(
        title: _title,
        home: const MainScreenWidget(),
        theme: getGoGameThemeData(),
        debugShowCheckedModeBanner: false)
  );
}
