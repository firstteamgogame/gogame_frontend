import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:io';

String url = 'https://go-game-team-1.herokuapp.com';

// via email and password

Future<http.Response> signUp(email, password) async{
  final response = await http.post(Uri.parse('$url/signup?email=$email&password=$password'),
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader : '',
        "Access-Control-Allow-Origin": "*",
      }
  );
  return response;
}

Future<http.Response> logIn(email, password) async{
  final response = await http.post(Uri.parse('$url/login?email=$email&password=$password'),
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader : '',
        "Access-Control-Allow-Origin": "*",
      }
  );
  return response;
}

Future<http.Response> logOut(email) async{
  final response = await http.post(Uri.parse('$url/logout?email=$email'),
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader : '',
        "Access-Control-Allow-Origin": "*",
      }
  );
  return response;
}
