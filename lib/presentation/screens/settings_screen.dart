import 'dart:math';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gogame/presentation/screens/rules_screen.dart';
import 'package:gogame/presentation/components/button.dart';
import 'package:gogame/presentation/screens/login_screen.dart';
import 'package:gogame/presentation/components/editable_person_card.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  int volumeValue = 50;
  final _random = Random();
  int next(int min, int max) => min + _random.nextInt(max - min);

  callAPI(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('login');
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const LoginScreen()),
    );
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Column(children: <Widget>[
          Stack(
            children: [
              Positioned(
                top: 40,
                left: 25,
                child: RotatedBox(
                  quarterTurns: 2,
                  child: IconButton(
                    icon: Icon(
                      Icons.logout,
                      size: 32,
                    ),
                    onPressed: () {
                      callAPI(context);
                    },
                  ),
                )
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40, bottom: 40),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SizedBox(
                    width: 240,
                    child: Text(
                      'Set up your own game',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        color: Theme.of(context).colorScheme.primary,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                ),
              ),
              Positioned(
                  top: 40,
                  right: 35,
                  child: CircleAvatar(
                      radius: 20,
                      backgroundColor: Theme.of(context).colorScheme.primary,
                      child: IconButton(
                        icon: const ImageIcon(
                          AssetImage('assets/images/scroll.png'),
                          color: Colors.white,
                          size: 24,
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RulesScreen()),
                          );
                        },
                      ))),
            ],
          ),
          Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: EditablePersonCard(
                  placeholder: 'Best Player',
                  avatar:
                  Image(image: AssetImage('assets/Avatar${next(1, 18)}.png')),
                  onInputChanged: (String value) {
                    print(value);
                  },
                ),
              ),
              Positioned(
                bottom: 0,
                left: 157,
                child: Text(
                  "rank #1",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 16,
                    color: Color(0xFF939192),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 20),
          const Padding(
            padding: EdgeInsets.only(left: 30),
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                'Board Design',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 24,
                  color: Colors.black,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, left: 30),
            child: Row(
              children: const [
                Padding(
                  padding: EdgeInsets.only(right: 15),
                  child: Image(
                    image: AssetImage('assets/images/board1.png'),
                    width: 50,
                    height: 50,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 15),
                  child: Image(
                    image: AssetImage('assets/images/board2.png'),
                    width: 50,
                    height: 50,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 15),
                  child: Image(
                    image: AssetImage('assets/images/board3.png'),
                    width: 50,
                    height: 50,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 15),
                  child: Image(
                    image: AssetImage('assets/images/board4.png'),
                    width: 50,
                    height: 50,
                  ),
                ),
                Image(
                  image: AssetImage('assets/images/board5.png'),
                  width: 50,
                  height: 50,
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 20, left: 30),
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                'Stone Design',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 24,
                  color: Colors.black,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, left: 30),
            child: Row(
              children: const [
                Padding(
                  padding: EdgeInsets.only(right: 15),
                  child: Image(
                    image: AssetImage('assets/images/stone1.png'),
                    width: 50,
                    height: 50,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 15),
                  child: Image(
                    image: AssetImage('assets/images/stone2.png'),
                    width: 50,
                    height: 50,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 15),
                  child: Image(
                    image: AssetImage('assets/images/stone3.png'),
                    width: 50,
                    height: 50,
                  ),
                ),
                Image(
                  image: AssetImage('assets/images/stone4.png'),
                  width: 50,
                  height: 50,
                ),
              ],
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(bottom: 30),
                child: Button(
                  buttonText: 'Save',
                  width: 280,
                  height: 56,
                  bgColor: Color(0xFF939192),
                  onPressed: () { },
                ),
              ),
            ),
          ),
        ]),
  );
}
