import 'dart:math';

import 'package:flutter/material.dart';

class StatisticsScreen extends StatelessWidget {
  final _random = new Random();
  int next(int min, int max) => min + _random.nextInt(max - min);
  @override
  Widget build(BuildContext context) => Expanded(
        child: Column(
          children: [
            SizedBox(height: 40),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(width: 50),
                  SizedBox(
                      child: Image(
                    image: AssetImage(
                      'assets/Back.png',
                    ),
                    width: 33,
                    height: 33,
                  )),
                  SizedBox(width: 50),
                  SizedBox(
                    child: Text(
                      'Statistics',
                      style: TextStyle(
                          fontSize: 36,
                          color: Color(0xFF396FED),
                          fontWeight: FontWeight.w900),
                    ),
                  )
                ]),
            SizedBox(height: 70),
            Text(
              '1030 points',
              style: TextStyle(
                  fontSize: 36,
                  color: Color(0xFF396FED),
                  fontWeight: FontWeight.w700),
            ),
            SizedBox(height: 10),
            Text(
              'rank #1',
              style: TextStyle(
                  fontSize: 36,
                  color: Color(0xFF848484),
                  fontWeight: FontWeight.w700),
            ),
            SizedBox(height: 60),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                      child: Image(
                    image: AssetImage(
                      'assets/black stone.png',
                    ),
                    width: 40,
                    height: 40,
                  )),
                  SizedBox(width: 14),
                  Text(
                    '89%',
                    style: TextStyle(
                        fontSize: 36,
                        color: Color(0xFF396FED),
                        fontWeight: FontWeight.w700),
                  ),
                  SizedBox(width: 54),
                  Image(
                    image: AssetImage(
                      'assets/white stone.png',
                    ),
                    width: 40,
                    height: 40,
                  ),
                  SizedBox(width: 14),
                  Text(
                    '98%',
                    style: TextStyle(
                        fontSize: 36,
                        color: Color(0xFF396FED),
                        fontWeight: FontWeight.w700),
                  ),
                ]),
            Text(
              'Games won with different\nstone color',
              style: TextStyle(
                  fontSize: 30,
                  color: Color(0xFF848484),
                  fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 50),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                      child: Image(
                    image: AssetImage(
                      'assets/Chart-bar.png',
                    ),
                    width: 39,
                    height: 39,
                  )),
                  SizedBox(width: 14),
                  Text(
                    '48',
                    style: TextStyle(
                        fontSize: 36,
                        color: Color(0xFF396FED),
                        fontWeight: FontWeight.w700),
                  ),
                  SizedBox(width: 80),
                  Image(
                    image: AssetImage(
                      'assets/Chart-pie.png',
                    ),
                    width: 39,
                    height: 39,
                  ),
                  SizedBox(width: 14),
                  Text(
                    '97%',
                    style: TextStyle(
                        fontSize: 36,
                        color: Color(0xFF396FED),
                        fontWeight: FontWeight.w700),
                  ),
                ]),
            SizedBox(height: 10),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                      child: Text(
                    'Average\nstones per\ngame',
                    style: TextStyle(
                        fontSize: 30,
                        color: Color(0xFF848484),
                        fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  )),
                  SizedBox(width: 30),
                  Text(
                    'Overall\ngames won',
                    style: TextStyle(
                        fontSize: 30,
                        color: Color(0xFF848484),
                        fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                ]),
          ],
        ),
      );
}
