import 'package:flutter/material.dart';

import 'package:gogame/presentation/components/button.dart';
import 'package:gogame/presentation/components/game_control_buttons.dart';
import 'package:gogame/presentation/screens/board_screen.dart';

class GameScreen extends StatefulWidget {
  // количество очков?
  @override
  State<GameScreen> createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  int caps1 = 0;

  int caps2 = 0;

  bool turn1 = false;

  bool turn2 = true;

  History history = History.none;

  @override
  Widget build(BuildContext context) => Expanded(
        child: Material(
            child: SingleChildScrollView(
                child: Padding(
          padding: EdgeInsets.only(top: 25),
          child: Column(
            children: [
              RotatedBox(
                  quarterTurns: 2,
                  child: Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(bottom: 5, left: 15),
                              child: Row(children: [
                                const Text(
                                  "Caps: ",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 24),
                                ),
                                Text(
                                  caps1.toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 21),
                                )
                              ]),
                            ),
                            Stack(
                              children: [
                                Row(children: [
                                  Padding(
                                    padding: EdgeInsets.only(right: 5, left: 15),
                                    child: Image(
                                      image: AssetImage('assets/images/stone1.png'),
                                      width: 50,
                                      height: 50,
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(right: 0),
                                      child: Text(
                                        "Player 1",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 21),
                                      )),
                                ]),
                                Positioned(
                                  left: -20,
                                  child: Icon(
                                    Icons.arrow_right_rounded,
                                    color: turn1 ? Color(0xFF2F80ED) : Colors.transparent,
                                    size: 50,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(bottom: 10, right: 15),
                              child: Button(
                                buttonText: "Territory",
                                width: 130,
                                height: 50,
                                onPressed: () {},
                                bgColor: Color(0xFF2F80ED),
                                radius: 10,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 10, right: 15),
                              child: Button(
                                buttonText: "Pass",
                                width: 130,
                                height: 50,
                                onPressed: () {},
                                bgColor: Color(0xFF828282),
                                radius: 10,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )),
              GameControlButtons(longPreviousButtonTapped: () {
                setState(() {
                  history = History.begin;
                });
              }, previousButtonTapped: () {
                setState(() {
                  history = History.previous;
                });
              }, nextButtonTapped: () {
                setState(() {
                  history = History.next;
                });
              }, longNextButtonTapped: () {
                setState(() {
                  history = History.last;
                });
              }),
              Padding(
                padding: EdgeInsets.only(top: 5, bottom: 5),
                child: BoardWidget(
                  history: history,
                ),
              ),
              GameControlButtons(longPreviousButtonTapped: () {
                setState(() {
                  history = History.begin;
                });
              }, previousButtonTapped: () {
                setState(() {
                  history = History.previous;
                });
              }, nextButtonTapped: () {
                setState(() {
                  history = History.next;
                });
              }, longNextButtonTapped: () {
                setState(() {
                  history = History.last;
                });
              }),
              Padding(
                padding: EdgeInsets.only(left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Stack(
                          children: [
                            Row(
                              children: const [
                                Padding(
                                  padding: EdgeInsets.only(right: 5, left: 15),
                                  child: Image(
                                    image: AssetImage('assets/images/white_stone.png'),
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 0),
                                  child: Text(
                                    "Player 2",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 21),
                                  ),
                                ),
                              ],
                            ),
                            Positioned(
                              left: -20,
                              child: Icon(
                                Icons.arrow_right_rounded,
                                color: turn2 ? Color(0xFF2F80ED) : Colors.transparent,
                                size: 50,
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            const Padding(
                                padding: EdgeInsets.only(right: 5, top: 5, left: 15),
                                child: Text(
                                  "Caps:",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 24),
                                )),
                            Padding(
                                padding: EdgeInsets.only(top: 5),
                                child: Text(
                                  caps2.toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 21),
                                )),
                          ],
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 10, right: 15),
                          child: Button(
                            buttonText: "Pass",
                            width: 130,
                            height: 50,
                            onPressed: () {},
                            bgColor: Color(0xFF828282),
                            radius: 10,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10, right: 15),
                          child: Button(
                            buttonText: "Territory",
                            width: 130,
                            height: 50,
                            onPressed: () {},
                            bgColor: Color(0xFF2F80ED),
                            radius: 10,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ))),
      );
}
