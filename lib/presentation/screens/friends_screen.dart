import 'dart:math';

import 'package:flutter/material.dart';
import 'package:gogame/presentation/components/badge.dart';
import 'package:gogame/presentation/components/editable_person_card.dart';
import 'package:gogame/presentation/components/go_game_button.dart';
import 'package:gogame/presentation/components/person_card.dart';
import 'package:gogame/presentation/screens/game_screen.dart';
import 'package:gogame/presentation/screens/rules_screen.dart';

class FriendsScreen extends StatelessWidget {
  final _random = Random();
  int next(int min, int max) => min + _random.nextInt(max - min);
  @override
  Widget build(BuildContext context) => Expanded(
    child: SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            width: 400,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 200,
                    height: 80,
                    child: Text(
                      'Play on this device',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        color: Theme.of(context).colorScheme.primary,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Positioned(
                    top: 0,
                    right: 5,
                    child: CircleAvatar(
                        radius: 20,
                        backgroundColor: Theme.of(context).colorScheme.primary,
                        child: IconButton(
                          icon: const ImageIcon(
                            AssetImage('assets/images/scroll.png'),
                            color: Colors.white,
                            size: 24,
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RulesScreen()),
                            );
                          },
                        ))),
              ],
            ),
          ),
          SizedBox(height: 20),
          PersonCard(
            title: 'Player1',
            avatar: Image(
              image: AssetImage(
                'assets/Avatar${next(1, 18)}.png',
              ),
            ),
            tags: [],
          ),
          SizedBox(height: 20),
          Text(
            'VS',
            style: Theme.of(context).textTheme.headline5,
          ),
          SizedBox(height: 20),
          EditablePersonCard(
            placeholder: 'Player 2',
            avatar:
                Image(image: AssetImage('assets/Avatar${next(1, 18)}.png')),
            onInputChanged: (String value) {
              print(value);
            },
          ),
          SizedBox(height: 20),
          Text(
            'Rules',
            style: Theme.of(context).textTheme.headline5,
          ),
          SizedBox(height: 10),
          Row(
            children: [
              Badge(text: 'Chinese', isHighlighted: true, onPressed: () {}),
              SizedBox(width: 10),
              Badge(text: 'Japanese', isHighlighted: false, onPressed: () {}),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Padding(
            padding: EdgeInsets.only(top: 50),
            child: GoGameButton(
              child: Text('Start a local game'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => GameScreen()),
                );
              },
            ),
          )
        ],
      ),
    ),
  );
}
