import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class SocketScreen extends StatefulWidget {
  SocketScreen({Key? key}) : super(key: key);

  @override
  _SocketScreenState createState() => _SocketScreenState();
}

class _SocketScreenState extends State<SocketScreen> {
  final contentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Socket screen'),
      ),
      body: FutureBuilder(
        future: Socket.connect('10.0.2.2', 4567),
        builder: (contex, AsyncSnapshot<Socket> snapshot) {
          if (!snapshot.hasData) return Column();
          final Socket socket = snapshot.data!;
          final Stream<Uint8List> stream = socket.asBroadcastStream();
          return Column(
            children: [
              // TextField(),
              TextField(controller: contentController),
              Center(
                child: StreamBuilder(
                  builder: (context, AsyncSnapshot<Uint8List> snapshot) {
                    return Text(
                        String.fromCharCodes(snapshot.data ?? List.empty()));
                  },
                  stream: stream,
                ),
              ),
              MaterialButton(
                onPressed: () {
                  final String currentValue = contentController.text;
                  socket.write(currentValue);
                },
                child: Text('Send'),
              )
            ],
          );
        },
      ),
    );
  }
}
