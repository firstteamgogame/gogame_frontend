import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gogame/presentation/components/text_input.dart';
import 'package:gogame/presentation/components/button.dart';
import 'package:gogame/presentation/screens/register_screen.dart';
import 'package:gogame/presentation/screens/main_screen.dart';
import 'package:gogame/utils/value_listenable_builder.dart';
import 'package:gogame/services/authorization.dart';
import 'dart:convert';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _loginFormKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  String? emailInput;
  String? passwordInput;
  String? emailErrorText;
  String? passwordErrorText;

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  callAPI(email, password, context){
    logIn(email, password).then((response) async {
      if (response.statusCode == 200) {
        _loginFormKey.currentState!.reset();
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool("login", true);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const MainScreenWidget()),
        );
      }
      else {
        final detail = jsonDecode(response.body)['detail'];

        if (detail == 'EMAIL_NOT_FOUND') {
          emailErrorText = 'This email is not registered';
        } else if (detail == 'INVALID_EMAIL') {
          emailErrorText = 'Invalid email';
        } else if (detail == 'INVALID_PASSWORD') {
          passwordErrorText = 'Invalid password';
        }
        _loginFormKey.currentState!.validate();
        emailErrorText = null;
        passwordErrorText = null;
      }
    }).catchError((error){
      print('error: $error');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _loginFormKey,
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.only(top: 60.0, left: 64.0, bottom: 30.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Login',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        color: Color(0xFF14142B),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 60.0, right: 60.0, bottom: 20.0),
                  child: TextInput(
                    label: 'Email',
                    hint: 'Enter your e-mail...',
                    errorText: emailErrorText,
                    controller: _emailController,
                    validator: (value) => emailErrorText,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 60.0, right: 60.0, bottom: 20.0),
                  child: TextInput(
                    label: 'Password',
                    hint: 'Enter your password...',
                    isPassword: true,
                    errorText: passwordErrorText,
                    controller: _passwordController,
                    validator: (value) => passwordErrorText,
                  ),
                ),
                ValueListenableBuilder2<TextEditingValue, TextEditingValue>(
                  _emailController,
                  _passwordController,
                  builder: (context, valueEmail, valuePassword) {
                    return Button(
                      buttonText: 'Login',
                      width: 280,
                      height: 56,
                      onPressed: (valueEmail.text.isNotEmpty && valuePassword.text.isNotEmpty)
                          ? () {
                            emailInput = valueEmail.text;
                            passwordInput = valuePassword.text;
                            if (_loginFormKey.currentState!.validate()) {
                              callAPI(emailInput, passwordInput, context);
                            }
                          }
                          : null,
                      bgColor: (valueEmail.text.isNotEmpty && valuePassword.text.isNotEmpty)
                          ? null
                          : const Color(0xFF8FB7ED),
                    );
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 100.0, right: 100.0, top: 10),
                  child: TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const RegisterScreen()),
                      );
                    },
                      child: const Text(
                        'Don’t have an account? Register now!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            decoration: TextDecoration.underline
                        ),
                      )
                  ),
                ),
              ],
            ),
          ),
        )
      )
    );
  }
}
