import 'dart:math';

import 'package:flutter/material.dart';
import 'package:gogame/presentation/components/go_game_button.dart';
import 'package:gogame/presentation/components/person_card.dart';

class ResultScreen extends StatelessWidget {
  final String playerName;
  final bool isWon;
  final bool isDraw;
  final String currentPlayerName;
  final String points;
  final _random = new Random();
  int next(int min, int max) => min + _random.nextInt(max - min);
  ResultScreen(
      {required this.playerName,
      this.isWon = true,
      this.isDraw = false,
      this.currentPlayerName = '',
      required this.points});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.withOpacity(0.5),
      appBar: AppBar(
        title: Text('Result'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 20),
          isDraw
              ? PersonCard(
                  title: 'Player2',
                  avatar: Image(
                    image: AssetImage(
                      'assets/Avatar${next(1, 18)}.png',
                    ),
                  ),
                  tags: [],
                )
              : SizedBox(width: 0, height: 0),
          SizedBox(height: 40),
          PersonCard(
            title: 'Player1',
            avatar: Image(
              image: AssetImage(
                'assets/Avatar${next(1, 18)}.png',
              ),
            ),
            tags: [],
          ),
          SizedBox(height: 80),
          Text(
            isDraw ? 'Draw' : 'Has won!',
            style: TextStyle(
              color: isDraw ? Color(0xFF848484) : Color(0xFF00BBC4),
              fontSize: 30,
              fontWeight: FontWeight.w800,
            ),
          ),
          SizedBox(height: 20),
          Text(
            '$points rating points',
            style: TextStyle(
                color: isDraw
                    ? Color(0xFF848484)
                    : (isWon ? Color(0xFF00BBC4) : Color(0xFFEF4151)),
                fontSize: 30,
                fontWeight: FontWeight.w800),
          ),
          SizedBox(height: 80),
          GoGameButton(
            child: Text('Go back to your games'),
            onPressed: () {},
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }
}
