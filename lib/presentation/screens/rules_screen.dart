import 'package:flutter/material.dart';

class RulesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: Stack(
                children: [
                  SizedBox(
                    height: 32,
                    width: 340,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        'Rules',
                        style: TextStyle(
                            color: Theme.of(context).colorScheme.primary,
                            fontWeight: FontWeight.w800,
                            fontSize: 32
                        ),
                      ),
                    )
                  ),
                  Positioned(
                    top: -5,
                    left: 0,
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Color(0xFF848484),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              child: const Text(
                  "Notes: The words move and territory are used differently here than elsewhere in this article; play and area, respectively, are used instead. A clarification to rule 5 is added in parentheses. The board is empty at the onset of the game (unless players agree to place a handicap). Black makes the first move, after which White and Black alternate. A move consists of placing one stone of one's own color on an empty intersection on the board. A player may pass their turn at any time. A stone or solidly connected group of stones of one color is captured and removed from the board when all the intersections directly adjacent to it are occupied by the enemy. (Capture of the enemy takes precedence over self-capture.) No stone may be played so as to recreate a former board position. Two consecutive passes end the game. A player's area consists of all the points the player has either occupied or surrounded. The player with more area wins. These rules rely on common sense to make notions such as connected group and surround precise. What is here called a solidly connected group of stones is also called a chain.",
                  style: TextStyle(fontSize: 22)),
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30)
            )
          ],
        ),
      ),
    );
}
