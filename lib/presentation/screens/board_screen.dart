import 'package:flutter/material.dart';
import 'package:gogame/presentation/components/board.dart';

class BoardWidget extends StatefulWidget {
  History history;
  @override
  State<BoardWidget> createState() => _BoardWidgetState(historyParam: history);
  BoardWidget({Key? key, required this.history}) : super(key: key);
}

enum Step { black, white }

enum CellState { white, black, none, whiteNone, blackNone }

enum History { next, last, previous, begin, none }

class CircleItem {
  double x;
  double y;
  double cellSize;
  late bool isInside;
  late CellState cellState;
  late final int xPos;
  late final int yPos;
  CircleItem(
      {required this.x, required this.y, required this.cellSize, this.cellState = CellState.none}) {
    xPos = x.toInt() ~/ cellSize.toInt();
    yPos = y.toInt() ~/ cellSize.toInt();
    isInside = false;
  }
  String toString() {
    return '$xPos $yPos $cellState';
  }
}

class _BoardWidgetState extends State<BoardWidget> {
  History historyParam;
  int currentIndex = 0;
  List<CircleItem> _circlesPos = [];
  List<List<CircleItem>> history = [];
  var _items = List.generate(
      9,
      (y) => List.generate(
            9,
            (x) => CircleItem(x: 15 * x.toDouble(), y: 15 * y.toDouble(), cellSize: 15),
          ),
      growable: false);

  bool currentStep = true;
  @override
  _BoardWidgetState({required this.historyParam});
  Widget build(BuildContext context) {
    print('history: $historyParam');
    print('history:${widget.history}');
    if (widget.history == History.none || currentIndex == history.length - 1) {
      List<CircleItem> tmpCopy = _circlesPos
          .map((CircleItem e) =>
              CircleItem(x: e.x, y: e.y, cellSize: e.cellSize, cellState: e.cellState))
          .toList();
      history.add(tmpCopy);
    }
    currentIndex = (widget.history == History.none && history.length > 0)
        ? (history.length - 1)
        : getArrayIndex(currentIndex, widget.history, history.length);
    checkingBoardCellState(_items);
    _circlesPos.forEach((CircleItem element) {
      _items[element.yPos][element.xPos] = element;
    });
    return Container(
      width: 315,
      height: 315,
      child: GestureDetector(
        onTap: () {
          //    print('-------tapped-------');
        },
        onTapUp: (detail) {
          //   print('------------onTapUp----------');
          //  print(detail.localPosition);
          double x = detail.localPosition.dx;
          double y = detail.localPosition.dy;
          onTouch(315, 315, x, y, 9);
        },
        child: CustomPaint(
          painter: BoardPainter(circlesCenters: history[currentIndex]),
        ),
      ),
    );
  }

  bool isSurrounded(int x, int y, List<List<CircleItem>> items) {
    CellState opositeColor = getOppositeColor(items[y][x].cellState);
    //  print('++++++++items: ${items} ${opositeColor} ${items[y][x].cellState}');
    bool horizontal = checker(x, y, items[y], opositeColor);
    print('vertical: ${horizontal} ${x} ${y} ${items[y]}');
    late bool downVerticalCheck = false;
    late bool upVerticalCheck = false;
    if (horizontal) {
      for (int i = y - 1; i > 0; i--) {
        if (items[i][x].cellState == opositeColor) {
          print('-----------SURROUNDED------down ${x} ${y}');
          downVerticalCheck = true;
          break;
        }
      }
      for (int i = y + 1; i < 9; i++) {
        if (items[i][x].cellState == opositeColor) {
          print('-----------SURROUNDED------up ${x} ${y}');
          upVerticalCheck = true;
          break;
        }
      }
      print('upVertical && downVerticalCheck ${upVerticalCheck} ${downVerticalCheck} ${x} ${y}');
      return upVerticalCheck && downVerticalCheck;
    } else {
      print('------not----surrounded----- ${x} ${y}');
      return false;
    }
  }

  int getArrayIndex(int currentIndex, History history, int length) {
    print('historyIndex: $history');
    if (history == History.begin) {
      return 0;
    }
    if (history == History.last) {
      if (length > 0) {
        return length - 1;
      } else {
        return 0;
      }
    }
    if (history == History.next) {
      if (currentIndex + 1 < length) {
        return currentIndex + 1;
      }
      return length - 1;
    }
    if (history == History.previous) {
      if (currentIndex - 1 >= 0) {
        return currentIndex - 1;
      }
      return currentIndex;
    }
    if (currentIndex + 1 > length - 1) {
      return currentIndex;
    }
    return currentIndex;
  }

  CellState getCheckedColor(CellState color) {
    switch (color) {
      case CellState.black:
        return CellState.blackNone;
      case CellState.white:
        return CellState.whiteNone;
      default:
        return color;
    }
  }

  bool checker(int x, int y, List<CircleItem> row, CellState color) {
    print('x: ${x}');
    late bool down = false;
    late bool up = false;
    for (int i = x - 1; i > 0; i--) {
      print('i,down: ${i} ${row[i].cellState}');
      if (row[i].cellState == color) {
        down = true;
      }
    }
    for (int i = x + 1; i < 9; i++) {
      print('i,up: ${i} ${row[i].cellState}');
      if (row[i].cellState == color) {
        up = true;
      }
    }
    return down && up;
  }

  bool verticalChecker(List<List<CircleItem>> arrayItems, int x, int y, CellState opositeColor) {
    late bool downVerticalCheck = false;
    late bool upVerticalCheck = false;
    for (int i = y - 1; i > 0; i--) {
      if (arrayItems[i][x].cellState == opositeColor) {
        print('-----------SURROUNDED------down ${x} ${y}');
        downVerticalCheck = true;
        break;
      }
    }
    for (int i = y + 1; i < 9; i++) {
      if (arrayItems[i][x].cellState == opositeColor) {
        print('-----------SURROUNDED------up ${x} ${y}');
        upVerticalCheck = true;
        break;
      }
    }
    return downVerticalCheck && upVerticalCheck;
  }

  void onTouch(double width, double height, double x, double y, int side) {
    double cellWidth = width / side;
    double cellHeight = height / side;

    double xPos = (x / cellWidth).round() * cellWidth;
    double yPos = (y / cellHeight).round() * cellHeight;
    print((y ~/ cellHeight.toInt()).round());
    setState(() {
      _circlesPos.add(
        CircleItem(
          x: xPos,
          y: yPos,
          cellSize: cellWidth,
          cellState: ((currentStep) ? CellState.white : CellState.black),
        ),
      );
      currentStep = !currentStep;
    });

    CellState test = ((currentStep) ? CellState.white : CellState.black);
    print('test ${test}');
    // int sideLeftRight = (x.toInt() % cellWidth.toInt() >= cellWidth / 2) ? 1 : 0;
    // int row = (x.toInt() ~/ cellWidth.toInt()) + sideLeftRight;
    // int sideUpDown = (x.toInt() % cellHeight.toInt() >= cellHeight / 2) ? 1 : 0;
    // int column = (x.toInt() ~/ cellHeight.toInt()) + sideUpDown;
    print('side $xPos $yPos');
    double xPosToCoordinate = xPos / cellWidth;
    double yPosToCoordinate = yPos / cellHeight;
    print('coordinate $xPosToCoordinate $yPosToCoordinate');
  }
}

// void onTouch(double width, double height, double x, double y, int side) {
//   double cellWidth = width / side;
//   double cellHeight = height / side;

//   int xPos = (x ~/ cellWidth.toInt()).round() * cellWidth.toInt();
//   int yPos = (y ~/ cellHeight.toInt()).round() * cellHeight.toInt();
//   // int sideLeftRight = (x.toInt() % cellWidth.toInt() >= cellWidth / 2) ? 1 : 0;
//   // int row = (x.toInt() ~/ cellWidth.toInt()) + sideLeftRight;
//   // int sideUpDown = (x.toInt() % cellHeight.toInt() >= cellHeight / 2) ? 1 : 0;
//   // int column = (x.toInt() ~/ cellHeight.toInt()) + sideUpDown;
//   print('side $xPos $yPos');
// }
void checkingBoardCellState(List<List<CircleItem>> board) {
  print('boardLength: ${board.length}');
  for (int y = 0; y < board.length; y++) {
    for (int x = 0; x < board[y].length; x++) {
      print('--------------${x} ${y}--------------------');
      CellState oppositeColor = getOppositeColor(board[y][x].cellState);
      board[y][x].isInside = isCellSurrounded(board, y, x, oppositeColor);
      print('element ${board[y][x].xPos} ${board[y][x].yPos} ${board[y][x].isInside}');
    }
  }
  // board.forEach(
  //   (List<CircleItem> element) {
  //     element.forEach(
  //       (CircleItem element) {
  //         CellState oppositeColor = getOppositeColor(element.cellState);
  //         element.isInside = isCellSurrounded(board, element.xPos, element.yPos, oppositeColor);
  //         print('element ${element.xPos} ${element.yPos} ${element.isInside}');
  //       },
  //     );
  //   },
  // );
}

bool isCellSurrounded(List<List<CircleItem>> board, int y, int x, CellState oppositeColor) {
  List<CircleItem> query = [board[y][x]];
  List<CircleItem> visited = [];
  while (query.isNotEmpty) {
    final CircleItem currentCell = query.first;
    query.removeAt(0);
    final CellState currentState = currentCell.cellState;
    final List<CircleItem> neighbours = [
      _getTopCell(board, currentCell.yPos, currentCell.xPos),
      _getLeftCell(board, currentCell.yPos, currentCell.xPos),
      _getBottomCell(board, currentCell.yPos, currentCell.xPos),
      _getRightCell(board, currentCell.yPos, currentCell.xPos)
    ].whereType<CircleItem>().toList();
    print('neighbours: $neighbours');
    for (final neighbour in neighbours) {
      if (neighbour.cellState == CellState.none) {
        return false;
      }
      if (neighbour.cellState == currentState && !visited.contains(neighbour)) {
        query.add(neighbour);
      }
      visited.add(currentCell);
    }
  }

  return true;
}

CircleItem? _getTopCell(List<List<CircleItem>> board, int y, int x) {
  if (y == 0) {
    return null;
  } else {
    return board[y - 1][x];
  }
}

CircleItem? _getLeftCell(List<List<CircleItem>> board, int y, int x) {
  if (x == 0) {
    return null;
  } else {
    return board[y][x - 1];
  }
}

CircleItem? _getRightCell(List<List<CircleItem>> board, int y, int x) {
  if (x == board.length - 1) {
    return null;
  } else {
    return board[y][x + 1];
  }
}

CircleItem? _getBottomCell(List<List<CircleItem>> board, int y, int x) {
  if (y == board.length - 1) {
    return null;
  } else {
    return board[y + 1][x];
  }
}

CellState getOppositeColor(CellState color) {
  switch (color) {
    case CellState.black:
      return CellState.white;
    case CellState.white:
      return CellState.black;
    default:
      return CellState.none;
  }
}
