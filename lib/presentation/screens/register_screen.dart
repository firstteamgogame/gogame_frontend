import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gogame/presentation/components/text_input.dart';
import 'package:gogame/presentation/components/button.dart';
import 'package:gogame/presentation/screens/login_screen.dart';
import 'package:gogame/presentation/screens/main_screen.dart';
import 'package:gogame/utils/value_listenable_builder.dart';
import 'package:gogame/services/authorization.dart';
import 'dart:convert';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _repeatedPasswordController = TextEditingController();

  String? emailInput;
  String? passwordInput;
  String? repeatedPasswordInput;
  String? emailErrorText;
  String? passwordErrorText;
  String? repeatedPasswordErrorText;

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _repeatedPasswordController.dispose();
    super.dispose();
  }

  callAPI(email, password, context){
    signUp(email, password).then((response) async {
      if (response.statusCode == 200) {
        _formKey.currentState!.reset();
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool('login', true);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const MainScreenWidget()),
        );
      }
      else {
        print(response.statusCode);
        final detail = jsonDecode(response.body)['detail'];

        if (detail == 'EMAIL_EXISTS') {
          emailErrorText = 'Email taken.';
        } else if (detail == 'INVALID_EMAIL') {
          emailErrorText = 'Invalid email';
        } else if (detail == 'WEAK_PASSWORD : Password should be at least 6 characters') {
          passwordErrorText = 'Password should be 6 characters long';
        }
        _formKey.currentState!.validate();
        emailErrorText = null;
        passwordErrorText = null;
        repeatedPasswordInput = null;
      }
    }).catchError((error){
      print('error: $error');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.only(top: 70.0, left: 64.0, bottom: 30.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Register',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        color: Color(0xFF14142B),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 60.0, right: 60.0, bottom: 20.0),
                  child: TextInput(
                    label: 'Email',
                    hint: 'Enter your e-mail...',
                    errorText: emailErrorText,
                    controller: _emailController,
                    validator: (value) => emailErrorText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 60.0, right: 60.0, bottom: 20.0),
                  child: TextInput(
                    label: 'Password',
                    hint: 'Enter your password...',
                    isPassword: true,
                    errorText: passwordErrorText,
                    controller: _passwordController,
                    validator: (value) => passwordErrorText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 60.0, right: 60.0, bottom: 20.0),
                  child: TextInput(
                    label: 'Repeat password',
                    hint: 'Repeat your password...',
                    isPassword: true,
                    errorText: repeatedPasswordErrorText,
                    controller: _repeatedPasswordController,
                    validator: (value) {
                      if (passwordInput == value) {
                        return null;
                      } else {
                        return 'Passwords do not match';
                      }
                    }
                  ),
                ),
                ValueListenableBuilder3<TextEditingValue, TextEditingValue, TextEditingValue>(
                  _emailController,
                  _passwordController,
                  _repeatedPasswordController,
                  builder: (context, valueEmail, valuePassword, valueRepeatedPassword) {
                    return Button(
                      buttonText: 'Register',
                      width: 280,
                      height: 56,
                      onPressed: (valueEmail.text.isNotEmpty && valuePassword.text.isNotEmpty && valueRepeatedPassword.text.isNotEmpty)
                        ? () {
                            emailInput = valueEmail.text;
                            passwordInput = valuePassword.text;
                            repeatedPasswordInput =
                                valueRepeatedPassword.text;
                            if (_formKey.currentState!.validate()) {
                              callAPI(emailInput, passwordInput, context);
                            }
                          }
                        : null,
                      bgColor: (valueEmail.text.isNotEmpty && valuePassword.text.isNotEmpty && valueRepeatedPassword.text.isNotEmpty)
                        ? null
                        : const Color(0xFF8FB7ED),
                    );
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 100.0, right: 100.0, top: 10.0),
                  child: TextButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const LoginScreen()),
                        );
                      },
                      child: const Text(
                        'Already have an account? Login now!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16,
                          decoration: TextDecoration.underline
                        ),
                      )
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
