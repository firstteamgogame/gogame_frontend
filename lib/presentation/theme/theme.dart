import 'package:flutter/material.dart';

ThemeData getGoGameThemeData() {
  ThemeData theme = ThemeData();
  return theme.copyWith(
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        elevation: MaterialStateProperty.all<double>(0),
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(Color(0xFF2F80ED)),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Color(0xFF2F80ED)),
          ),
        ),
      ),
    ),
    colorScheme: theme.colorScheme.copyWith(
      primary: Color(0xFF2F80ED),
      secondary: Color(0xFF396FED),
      error: Color(0xFFC30052),
    ),
    bottomAppBarColor: Color(0xFF396FED),
    bottomAppBarTheme: theme.bottomAppBarTheme.copyWith(
      color: Color(0xFF396FED),
    ),
    unselectedWidgetColor: Color(0xFF939192),
    textTheme: theme.textTheme.copyWith(
      headline5: theme.textTheme.headline5!.copyWith(
        color: Color(0xFF396FED),
        fontWeight: FontWeight.w800,
      ),
      headline6: theme.textTheme.headline5!.copyWith(
        fontWeight: FontWeight.w700,
      ),
      caption: theme.textTheme.headline5!.copyWith(
        color: Color(0xFF396FED),
        fontWeight: FontWeight.w700,
      ),
      button: theme.textTheme.button!.copyWith(
        fontWeight: FontWeight.w500,
        fontSize: 20,
      ),
    ),
  );
}
