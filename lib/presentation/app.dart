import 'package:flutter/material.dart';
import 'package:gogame/presentation/screens/main_screen.dart';
import 'package:gogame/presentation/screens/login_screen.dart';
import 'package:gogame/presentation/screens/register_screen.dart';
import 'package:gogame/presentation/theme/theme.dart';

class GoGameApp extends StatelessWidget {
  const GoGameApp({Key? key}) : super(key: key);

  static const String _title = 'GoGame';

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: _title,
      home: const MainScreenWidget(),
      theme: getGoGameThemeData(),
      debugShowCheckedModeBanner: false,
    );
  }
}
