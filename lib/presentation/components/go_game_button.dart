import 'package:flutter/material.dart';

class GoGameButton extends StatelessWidget {
  final Widget child;
  final Function() onPressed;
  const GoGameButton({Key? key, required this.child, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 36,
          vertical: 16,
        ),
        child: child,
      ),
    );
  }
}
