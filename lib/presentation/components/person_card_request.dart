import 'package:flutter/material.dart';

class PersonCardRequest extends StatelessWidget {
  String title;
  String rank;
  String nation;
  String boards;
  final Widget avatar;

  PersonCardRequest({
    this.title = 'Another one',
    this.rank = 'rank #1',
    this.nation = 'Chinese',
    this.boards = '9x9  13x13',
    required this.avatar,
  });
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(width: 30),
        SizedBox(
          child: ClipOval(
            child: avatar,
          ),
          height: 80,
          width: 80,
        ),
        const SizedBox(
          width: 13,
        ),
        SizedBox(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style:
                    const TextStyle(fontSize: 30, fontWeight: FontWeight.w700),
              ),
              SizedBox(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      rank,
                      style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Color(0xFF396FED)),
                    ),
                    SizedBox(width: 9),
                    Text(
                      nation,
                      style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Color(0xFF396FED)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      boards,
                      style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Color(0xFF396FED)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
