import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gogame/presentation/screens/board_screen.dart';

class BoardObject {
  Offset c = Offset(0, 0);
  double radius;
  bool isOqupied = false;
  BoardObject({required this.c, this.radius = 10});
  @override
  String toString() {
    return c.toString();
  }
}

class BoardPainter extends CustomPainter {
  int boardSize = 9;
  double boardWidth = 315;
  List<CircleItem> circlesCenters = [];
  BoardPainter({required this.circlesCenters});

  @override
  void paint(Canvas canvas, Size size) {
    print('------------------------------');
    print(circlesCenters.toString());
    print('------------------------------');

    double edgeLength = boardWidth / boardSize;
    List<BoardObject> center = [];
    double x = edgeLength;
    double y = edgeLength;
    for (int i = 0; i < boardSize - 1; i++) {
      for (int j = 0; j < boardSize - 1; j++) {
        center.add(BoardObject(c: Offset(x, y)));
        x += edgeLength;
      }
      x = edgeLength;
      y += edgeLength;
    }

    // print(center.join());
    Rect rect = Rect.fromLTRB(0, 0, boardWidth, boardWidth);
    Paint brush = Paint()..color = Color(0xFFEBC677);
    Paint brushLines = Paint()
      ..color = Color(0xFF333333)
      ..strokeWidth = 3;
    canvas.drawRect(rect, brush);
    double startPosVerticalX = 0;
    double startPosVerticalY = 0;
    Offset endPosVertical = Offset(0, edgeLength);
    int startPosLine = 0;
    for (int i = 0; i < boardSize; i++) {
      canvas.drawLine(Offset(startPosVerticalX, 0), Offset(startPosVerticalX, 315), brushLines);
      canvas.drawLine(Offset(0, startPosVerticalY), Offset(315, startPosVerticalY), brushLines);
      startPosVerticalX += edgeLength;
      startPosVerticalY += edgeLength;
    }

    circlesCenters.forEach((element) {
      CellState color = (element.isInside) ? getCheckedColor(element.cellState) : element.cellState;
      Paint paintCircle = Paint()..color = getColor(color);
      // Paint paintCircle = Paint()..color = Color(0xFFFFFFFF);
      canvas.drawCircle(Offset(element.x.toDouble(), element.y.toDouble()), 10, paintCircle);
    });

    // center.forEach((BoardObject element) {
    //   canvas.drawCircle(element.c, element.radius, paintCircle);
    // });
  }

  bool shouldRepaint(CustomPainter oldDegree) {
    return true;
  }
}

CellState getCheckedColor(CellState color) {
  switch (color) {
    case CellState.black:
      return CellState.blackNone;
    case CellState.white:
      return CellState.whiteNone;
    default:
      return color;
  }
}

Color getColor(CellState state) {
  switch (state) {
    case CellState.black:
      return Color(0xFF000000);
      break;
    case CellState.blackNone:
      return Color(0xFF0000FF);
      break;
    case CellState.whiteNone:
      return Color(0xFFFFC0CB);
      break;
    default:
      return Color(0xFFFFFFFF);
  }
}

void onTouch(double width, double height, double x, double y, int side) {
  double cellWidth = width / side;
  double cellHeight = height / side;
  int sideLeftRight = (x.toInt() % cellWidth.toInt() >= cellWidth / 2) ? 1 : 0;
  int row = (x.toInt() ~/ cellWidth.toInt()) + sideLeftRight;
  int sideUpDown = (x.toInt() % cellHeight.toInt() >= cellHeight / 2) ? 1 : 0;
  int column = (x.toInt() ~/ cellHeight.toInt()) + sideUpDown;
  //('side $row $column');
}
