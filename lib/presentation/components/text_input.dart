import 'package:flutter/material.dart';

class TextInput extends StatefulWidget {
  final String label;
  final String hint;
  dynamic errorText;
  bool? isPassword;
  final TextEditingController controller;
  final String? Function(String?)? validator;

  TextInput({
    required this.label,
    required this.hint,
    required this.errorText,
    this.isPassword = false,
    required this.controller,
    required this.validator,
  });

  @override
  _TextInputState createState() => new _TextInputState();
}

class _TextInputState extends State<TextInput> {
  bool _passwordVisible = false;
  bool _valueFilled = false;

  void initState() {
    _passwordVisible = false;
    _valueFilled = false;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
        children: [
          TextFormField(
            controller: widget.controller,
            validator: widget.validator,
            onChanged: (value) {
              setState(() {
                _valueFilled = value != '';
              });
            },
            style: TextStyle(
              color: Colors.black,
              fontSize: 18,
            ),
            textAlignVertical: TextAlignVertical.bottom,
            obscureText: (widget.isPassword ?? false) && !_passwordVisible,
            decoration: InputDecoration(
              filled: true,
              fillColor: Color(0xFFEFF0F6),
              hintText: widget.hint,
              errorText: widget.errorText,
              hintStyle: TextStyle(
                color: Color(0xFF828282),
                fontSize: 18,
              ),
              suffixIcon: Icon(
                  Icons.clear,
                  color: Colors.transparent,
              ),
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding: EdgeInsets.only(left: 64, top: 36, bottom: 10),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                borderSide: BorderSide(width: 2, color: Theme.of(context).colorScheme.primary),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                borderSide: BorderSide(width: 2, color: Color(0xFFEFF0F6)),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                borderSide: BorderSide(width: 2,)
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                borderSide: BorderSide(width: 2, color: Theme.of(context).colorScheme.error)
              ),
            ),
          ),
          Positioned(
            child: Padding(child: Text(
              widget.label,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: widget.errorText != null ? Theme.of(context).colorScheme.error : Color(0xFF6E7191),
              )),
              padding: EdgeInsets.only(left: 64, top: 7, right: 64,),
            )
          ),
          Positioned(
            top: 10,
            left: 15,
            child: IconButton(
                icon: Icon(
                  widget.label == 'Email' ? Icons.email_outlined : Icons.remove_red_eye_outlined,
                  color: Color(0xFF14142B),
                  size: 24
                ),
              onPressed: () {
                // Update the state i.e. toggle the state of passwordVisible variable
                setState(() {
                  _passwordVisible = !_passwordVisible;
                });
              },
            )
          ),
          if (_valueFilled == true) Positioned(
            top: 10,
            right: 15,
            child: IconButton(
              onPressed: () {
                widget.controller.clear();
                setState(() {
                  _valueFilled = false;
                });
              },
              icon: Icon(
                Icons.clear,
                color:Color(0xFFA0A3BD),
              ),
            ),
          ),
          Padding(
            child: Text(
              widget.errorText ?? '',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: Theme.of(context).colorScheme.error,
              )),
            padding: EdgeInsets.only(left: 64, top: 5, right: 64,),
          )
        ]
    );
  }
}
