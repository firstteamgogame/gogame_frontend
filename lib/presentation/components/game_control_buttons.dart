import 'package:flutter/material.dart';

class GameControlButtons extends StatelessWidget {
  final void Function() longPreviousButtonTapped;
  final void Function() previousButtonTapped;
  final void Function() nextButtonTapped;
  final void Function() longNextButtonTapped;

  GameControlButtons(
      {required this.longPreviousButtonTapped,
      required this.previousButtonTapped,
      required this.nextButtonTapped,
      required this.longNextButtonTapped});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 16, right: 16),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              icon: Image.asset('assets/images/longPrevious.png'),
              color: Colors.white,
              onPressed: longPreviousButtonTapped,
              iconSize: 32,
            ),
            IconButton(
              icon: Image.asset('assets/images/previous.png'),
              onPressed: previousButtonTapped,
              iconSize: 32,
            ),
            IconButton(
              icon: Image.asset('assets/images/next.png'),
              onPressed: nextButtonTapped,
              iconSize: 32,
            ),
            IconButton(
              icon: Image.asset('assets/images/longNext.png'),
              color: Colors.white,
              onPressed: longNextButtonTapped,
              iconSize: 32,
            ),
          ]
      ),
    );
  }
}
