import 'package:flutter/material.dart';

class Badge extends StatefulWidget {
  final String text;
  final bool isHighlighted;
  final void Function() onPressed;
  Badge(
      {required this.text,
      required this.isHighlighted,
      required this.onPressed});

  @override
  State<Badge> createState() =>
      _Badge(text: text, isHighlighted: isHighlighted, onPressed: onPressed);
}

class _Badge extends State<Badge> {
  String text;
  bool isHighlighted;
  final void Function() onPressed;
  _Badge(
      {required this.text,
      required this.isHighlighted,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: ActionChip(
        label: Text(text),
        onPressed: onPressed,
        backgroundColor: isHighlighted ? Color(0xffe9dcf8) : Color(0xfff0f0f0),
        labelStyle: isHighlighted
            ? TextStyle(color: Color(0xff8045d2), fontSize: 14)
            : TextStyle(color: Color(0xffacacac), fontSize: 14),
      ),
    );
  }
}
