import 'package:flutter/material.dart';
import 'package:gogame/presentation/components/badge.dart';
import 'package:tuple/tuple.dart';

class BadgesBar extends StatelessWidget {
  final List<Tuple3<String, bool, void Function()>> settings;

  BadgesBar({required this.settings});
  List<Widget> widgets = [];
  @override
  Widget build(BuildContext context) {
    for (var setting in settings) {
      widgets.add(new Badge(
          text: setting.item1,
          isHighlighted: setting.item2,
          onPressed: setting.item3));
    }
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Wrap(
        spacing: 10,
        runSpacing: 10,
        children: widgets,
      ),
    );
  }
}
