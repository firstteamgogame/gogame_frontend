import 'package:flutter/material.dart';

class PersonCard extends StatelessWidget {
  String title;
  final Widget avatar;
  final List<String> tags;

  PersonCard({required this.title, required this.avatar, required this.tags});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          child: ClipOval(
            child: avatar,
          ),
          height: 80,
          width: 80,
        ),
        SizedBox(
          width: 20,
        ),
        SizedBox(
          child: Column(
            children: [
              Text(
                title,
                style: Theme.of(context).textTheme.headline6,
              ),
              Wrap(
                children: tags
                    .map(
                      (e) => Text(
                        e,
                        style: Theme.of(context).textTheme.caption,
                      ),
                    )
                    .toList(),
                spacing: 10,
              )
            ],
            crossAxisAlignment: CrossAxisAlignment.stretch,
          ),
          width: 200,
        ),
      ],
    );
  }
}
