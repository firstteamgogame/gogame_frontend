import 'package:flutter/material.dart';

class EditablePersonCard extends StatefulWidget {
  final String placeholder;
  final Widget avatar;
  final Function(String) onInputChanged;
  EditablePersonCard(
      {required this.placeholder,
      required this.avatar,
      required this.onInputChanged});

  @override
  State<EditablePersonCard> createState() => _EditablePersonCardState(
      placeholder: placeholder, avatar: avatar, onInputChanged: onInputChanged);
}

class _EditablePersonCardState extends State<EditablePersonCard> {
  String placeholder;
  final Widget avatar;
  final Function(String) onInputChanged;
  late FocusNode personNameFocusNode;
  _EditablePersonCardState(
      {required this.placeholder,
      required this.avatar,
      required this.onInputChanged});

  @override
  void initState() {
    super.initState();
    personNameFocusNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          child: ClipOval(
            child: avatar,
          ),
          height: 80,
          width: 80,
        ),
        SizedBox(
          width: 20,
        ),
        SizedBox(
          width: 200,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: TextField(
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    hintText: placeholder,
                  ),
                  focusNode: personNameFocusNode,
                  onChanged: onInputChanged,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              IconButton(
                icon: Icon(Icons.edit_outlined),
                onPressed: () {
                  personNameFocusNode.requestFocus();
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    personNameFocusNode.dispose();
    super.dispose();
  }
}
