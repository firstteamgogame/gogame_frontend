import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  String buttonText;
  double width;
  double height;
  double radius;
  void Function()? onPressed;
  Color? bgColor;
  Color fontColor;
  Widget? child;

  Button({
    required this.buttonText,
    required this.width,
    required this.height,
    this.radius = 16,
    required this.onPressed,
    this.bgColor,
    this.fontColor = Colors.white,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child: SizedBox(
        width: width,
        height: height,
        child: TextButton(
          style: TextButton.styleFrom(
            primary: fontColor,
            backgroundColor: bgColor ?? Theme.of(context).colorScheme.primary,
            textStyle: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
          ),
          onPressed: onPressed,
          child: child ?? Text(buttonText)
        )
      )
    );
  }
}
